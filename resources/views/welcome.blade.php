<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel Test task</title>

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .items-wrap{
                height: 90vh;
                display: flex;
                width: 100%;
                justify-content: space-around;
                align-items: center;
                font-size: 30px;
            }
            .item{
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
            }
            .item-image{
                max-width: 300px;
                max-height: 300px;
            }
            .item-image img{
                width: 100%;
                height: 100%;
                object-fit: cover;
            }
            .currencys{
                display: flex;
                flex-direction: column;
                align-items: center;
            }
            .paginate-arr{
                display: flex;
                width: 100%;
                justify-content: center;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

           @isset($items)
                <section class="items-wrap">
                    <?php $counter = 0 ?>
                @foreach ($items as $item)
                    <div class="item">
                        <form action="/del" method="POST">
                            @csrf
                            <div class="item-name">{{$item->name}}</div>
                                <div class="item-image">
                                    <img src="{{ url('storage/images/'.$item->imgsrc)}}" alt="">
                                </div>
                                <div class="currencys">
                                    <div>{{$currency[$counter]->grn}} грн.</div>
                                    <div>{{$currency[$counter]->usd}} usd.</div>
                                    <div>{{$currency[$counter]->eur}} eur.</div>
                                    <div>
                                        <input type="hidden" name="item-id" value="<?php echo $item->id?>">
                                        @if (Auth::check())
                                            <button type="submit" class="btn btn-danger">Видалити</button>
                                        @endif
                                        @isset($id)
                                        <?php
                                        echo "2233";  
                                        echo $id;?>
                                    @endisset
                                    </div>
                                </div>
                        </form>
                    </div>
                    <?php $counter++;?>
                @endforeach
                </section>
               <div class="paginate-arr">
                {{$items->links()}}
               </div>
               
           @endisset
    </body>
</html>
