<header>
    <style>
        .form-add{
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .form-item{
            display: flex;
            flex-direction: column;
            width: 100%;
            justify-content: center;
            align-items: center;
            margin-bottom: 2%;
        }
        .form-item_text{
            font-size: 20px;
            font-weight: bold;
            color: #38C172;
        }
        .form-item input[type=text]{
            width: 25%;
        }
        .error{
            position: fixed;
            bottom:0;
            width: 100%;
        }
    </style>
</header>


@extends('layouts.app')

<section class="form-add">
    <?php
    if(isset($some)){ 
        echo 22;
        dd($some);
    }
?>

    <form action="<?php echo url()->current();?>" method="post" enctype="multipart/form-data" id="form">
        @csrf
        <div class="display-4" align="center">
            Завантаження нових даних
        </div>
        <div class="form-item">
            <div class="form-item_text">Назва товару</div>
            <input type="text" name="item-name">
        </div>
        <div class="form-item">
            <div class="form-item_text">Додати зображення товару</div>
            <input type="file" name="image">
        </div>
        <div class="form-item">
            <div class="form-item_text">Ціна в гривнях</div>
            <input type="text" name="amount">
        </div>
        <div class="form-item">
            <button type="submit" class="btn btn-success">Завантажити дані</button>
        </div>
    </form>
</section>




<section class="error">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</section>

