<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormDbRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|image|mimes:jpeg,png',
            'item-name' => 'required|min:3',
            'amount' => 'required|numeric',
        ];
    }

    public function messages(){
        return [
            'image.required' => 'Потрібно вибрати файл',
            'image.image' => 'Потрібно вибрати зображення',
            'image.mimes' => 'Неправильний тип файлу, виберіть файл типу jpeg або png',
            'item-name.required' => 'Потрібно ввести назву товару',
            'item-name.min' => 'Потрібно ввести назву не меншу ніж :min символів',
            'amount.required' => 'Потрібно ввести ціну товару',
            'amount.numeric' => 'Ціна товару може бути тільки в числах з комою',
        ];
    }
}
