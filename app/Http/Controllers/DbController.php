<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Http\Requests\FormDbRequest;
use App\Item;
use Illuminate\Http\Request;

class DbController extends Controller
{
    public function add(FormDbRequest $request){
        $validated = $request->validated();
        $path = $request->file('image')->store('');
        Item::create(['name'=>''.$request->input('item-name'),'imgsrc'=>''.$path]);
        $client = new \GuzzleHttp\Client();
        $requestt = $client->get("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5");
        $response = json_decode($requestt->getBody());
        $usd=$request->input('amount')/$response[0]->buy;
        $grn=$request->input('amount');
        $eur=$grn/$response[1]->buy;
        Currency::create(['grn'=>''.$grn,'usd'=>''.$usd,'eur'=>''.$eur]);
        return redirect()->action('DbController@paginate');
    }

    public function paginate(){
        $items = Item::simplePaginate(3);
        $currency = Currency::simplePaginate(3);
        return view('welcome',compact('items','currency'));
    }

    public function del(Request $request){
        $id = $request->input('item-id');
        Item::destroy($id);
        Currency::destroy($id);
        return redirect()->action('DbController@paginate');
    }
}
