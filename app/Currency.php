<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $guarded = [];


    public function currency(){
        return $this->hasOne('App\Item','id','id');
    }
}
